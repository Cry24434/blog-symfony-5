<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory as Factory;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $users = [];
        for ($i=0; $i < 50; $i++) { 
            $user = new User();
            $user->setUsername($faker->name());
            $user->setFirstName($faker->firstName());
            $user->setLastname($faker->lastName());
            $user->setEmail($faker->email());
            $user->setPassword($faker->lastName());
            $user->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($user);
            $users[] = $user;
        }

        $categories = [];
        for ($i=0; $i < 10; $i++) { 
            $category = new Category();
            $category->setTitle($faker->text(50));
            $category->setDescription($faker->text(250));
            $category->setImage("https://via.placeholder.com/150/" . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT) . "/");
            $manager->persist($category);
            $categories[] = $category;
        }

        for ($i=0; $i < 100; $i++) { 
            $article = new Article();
            $article->setTitle($faker->text(50));
            $article->setContent($faker->text(350));
            $article->setImage("https://via.placeholder.com/150/" . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT) . "/");
            $article->setCreatedAt(new \DateTimeImmutable());
            $article->addCategory($categories[rand(0, 9)]);
            $article->setAuthor($users[rand(0, 49)]);
            $manager->persist($article);
        }



        $manager->flush();
    }
}
