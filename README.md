**Blog sous Symfony 5**

Pour utiliser le projet, il faut avoir :
- [Symfony](https://symfony.com/download)
- [Composer](https://getcomposer.org/download/)

**Installation**

`composer install`

Creer un fichier .env et y definir les variables APP_SECRET et DATABASE_URL:

```
APP_SECRET=toto
DATABASE_URL="mysql://USER:MOT_DE_PASSE@127.0.0.1:3306/DATABASE_NAME?DB_VERSION&charset=utf8mb4"
```

Pour creer la base de donnees
`symfony console doctrine:database:create`

Pour charger la migration
`symfony console doctrine:migration:migrate`

(optionnel) Pour appeler le faker de donnees
`symfony console doctrine:fixtures:load`

Enjoy
